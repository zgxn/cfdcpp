/*!
Class that solves the 2 dimensional nonlinear advection equations, 
that are two coupled partial differential equations written as:
d_t u + u d_x u + u d_y u = 0
d_t v + u d_x v + v d_y v = 0

The spatial derivatives are solved via backward differencing, 
the second order derivative is solved with the Taylor 
approximation.

Boundaries are periodical.

@author il
*/

#include "NonLinearAdvection2D.hpp"
#include <omp.h>

/*!
Construct 2 dimensional non linear advection equation

Input:
	xL : left most coordinate x
	xR : right most coordinate x
	yL : left most coordinate y
	yR : right most coordinate y
	nx : number of nodes in x direction
	ny : number of nodes in y direction
*/
NonLinearAdvection2D::NonLinearAdvection2D(double xL, double xR, double yL, 
double yR, int nx, int ny)
{
	mnx = nx;
	mny = ny;
	mdx = (xR - xL) / nx;
	mdy = (yR - yL) / ny;
}

/*
Update the whole domain

Overwrite the input vector of data

Input:
	u  : Vector of data in x-direction [INPUT/OUTPUT]
	v  : Vector of data in y-direction [INPUT/OUTPUT]
	dt : Time step size

Output:
	u  : Vector of data in x-direction updated to t+dt
	v  : Vector of data in y-direction updated to t+dt
*/
void NonLinearAdvection2D::solve(double** u, double** v, double dt)
{

	// Initialise the grid
	double** uN;
	double** vN;
	uN = new double* [mnx];
	vN = new double* [mnx];
	for (int i = 0; i < mnx; i ++) {
		uN[i] = new double [mny];
		vN[i] = new double [mny];
	}

	// Update
#pragma omp parallel for
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {

			// Boundary conditions
			if ( i==0 and j!=0 ) {
				uN[0][j] = update(dt, u[i][j], v[i][j], u[mnx-1][j], u[i][j-1]);
				vN[0][j] = update(dt, u[i][j], v[i][j], v[mnx-1][j], v[i][j-1]);
			} else if ( j==0 and i!=0 ) {
				uN[i][0] = update(dt, u[i][j], v[i][j], u[i-1][j], u[i][mny-1]);
				vN[i][0] = update(dt, u[i][j], v[i][j], v[i-1][j], v[i][mny-1]);
			} else if ( j==0 and i==0 ) {
				uN[0][0] = update(dt, u[i][j], v[i][j], u[mnx-1][j], u[i][mny-1]);
				vN[0][0] = update(dt, u[i][j], v[i][j], v[mnx-1][j], v[i][mny-1]);
			} else {
				// Inside the domain
				uN[i][j] = update(dt, u[i][j], v[i][j], u[i-1][j], u[i][j-1]);
				vN[i][j] = update(dt, u[i][j], v[i][j], v[i-1][j], v[i][j-1]);
			}

		}
	}

	// Overwrite the old vector of variables
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {
			u[i][j] = uN[i][j];
			v[i][j] = vN[i][j];
		}
	}

	// Clear memory
	for (int i = 0; i < mnx; i++) {
		delete[] uN[i];
		delete[] vN[i];
	}

}

/*!
Advance solution in time

Input:
	dt     : time step size
	u      : data at point i, j (in x-direction)
	v      : data at point i, j (in y-direction)
	uPrevX : data at point i-1, j
	uPrevY : data at point i, j-1

Output:
	Value of data at time t+1 at point (i, j)
*/
double NonLinearAdvection2D::update(double dt, double u, double v, 
	double uPrevX, double uPrevY)
{
	return u - u*dt*backwardDifference(u, uPrevX, mdx)
		- v*dt*backwardDifference(u, uPrevY, mdy);
}

/*!
Calculate first order differential by backward diff

Input:
	u     : data at point i
	uPrev : data at point i-1
	dL    : distance between points

Output:
	d_L u
*/
double NonLinearAdvection2D::backwardDifference(double u, double uPrev, 
double dL)
{
	return ( u - uPrev ) / dL;
}
