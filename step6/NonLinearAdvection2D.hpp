/*!
Class that solves the 2 dimensional nonlinear advection equations, 
that are two coupled partial differential equations written as:
d_t u + u d_x u + u d_y u = 0
d_t v + u d_x v + v d_y v = 0

The spatial derivatives are solved via backward differencing, 
the second order derivative is solved with the Taylor 
approximation.

Boundaries are periodical.

@author il
*/

class NonLinearAdvection2D
{

public:
NonLinearAdvection2D(double xL, double xR, double yL, double yR, 
int nx, int ny);
void solve(double** u, double** v,  double dt);

private:
int mnx;
int mny;
double mdx;
double mdy;
double backwardDifference(double u, double uPrev, double dL);
double update(double dt, double u, double v, double uPrevX, double uPrevY);

};
