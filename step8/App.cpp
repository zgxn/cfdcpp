/*!
	Starter object to solve the viscous Burgers' equations in 
	two dimensions.

	@author il
 */

#include "Burgers2D.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <omp.h>

int main(int argc, char* argv[])
{

	// --------------------------------------------------------------------------
	// Variable declaration
	// --------------------------------------------------------------------------
	int nx = 41; // number of nodes in x-direction
	int ny = 41; // number of nodes in y-direction
	double dx = 2.0/(nx-1); // mesh spacing in x-direction
	double dy = 2.0/(ny-1); // mesh spacing in y-direction
	int nt = 120; // time step count

	// numerical stability
	double sigma = 0.0009;
	double nu = 0.01;
	double dt = sigma*dx*dy/nu;

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double** u0;
	double** v0;

	u0 = new double* [ny];
	v0 = new double* [ny];

	for (int i = 0; i < nx; i ++) {
		u0[i] = new double [nx];
		v0[i] = new double [nx];
	}

	double x = 0.0;
	double y = 0.0;

#pragma omp parallel for
	for (int i = 0; i < ny; i++) {
		for (int j = 0; j < nx; j++) {
			x = j * dx;
			y = i * dy;
			if (0.5 <= y and y < 1.0 and 0.5 <= x and x < 1.0) {
				u0[i][j] = 2.0;
				v0[i][j] = 2.0;
			} else {
				u0[i][j] = 1.0;
				v0[i][j] = 1.0;
			}
		}
	}

	// --------------------------------------------------------------------------
	// Object creation
	// --------------------------------------------------------------------------
	Burgers2D bur (0.0, 2.0, 0.0, 2.0, nx, ny, nu);

	// --------------------------------------------------------------------------
	// Advance in time
	// --------------------------------------------------------------------------
	int t0 = 0; // Starting time

	while (t0 < nt) {

		if (t0 % 10 == 0) {
			std::cout << t0 << " | Still running...\n";
		}

		bur.solve(u0, v0, dt);
		t0 = t0 + 1;

	}

	bur.clearMemory();

	std::cout << t0 << " | Simulation finished.\n";

	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());

	for (int i=0; i<ny; i++) {
		for (int j=0; j<nx; j++) {
			write_output << (i*dy) << ", " << (j*dx) << ", " << u0[i][j]
				<< ", " << v0[i][j] << "\n";
		}
	}

	write_output.close();

	return 0;
}
