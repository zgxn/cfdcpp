/*!
Class that solves the 2 dimensional Burgers' equation. In 
two dimensions, Burgers' equation becomes a coupled set of 
partial differential equations (PDE):

d_t u + u d_x u + v d_y u = nu (d^2_x u + d^2_y u) -> [1]
d_t v + u d_x v + v d_y v = nu (d^2_x v + d^2_y v) -> [2]

Burgers' equation creates shocks from initially smooth data. 
In fact, the equations we are solving are the so-called 
"viscous Burgers' equations", because they contain the 
dissipative right side.

@author il
*/

#ifndef __BURGER_INCLUDED__
#define __BURGER_INCLUDED__

#include "FiniteDifferences.hpp"

class Burgers2D
{

	public:
		Burgers2D(double xL, double xR, double yL, double yR,
				int nx, int ny, double nu);
		void solve(double** u, double** v, double dt);
		void clearMemory();
		
	private:
		FiniteDifferences fdm;
		int mnx;
		int mny;
		double mdx;
		double mdy;
		double mnu;
		double** muN;
		double** mvN;
		double update(double dt, double q, double u, double v, 
				double qNextX, double qPrevX, double qNextY, double qPrevY,
				double dx, double dy, double nu);

};

#endif
