# -----------------------------------------------------------------------------
# Plot 2D data as a 3D projection
# Implemented as seen at
# http://lorenabarba.com/blog/cfd-python-12-steps-to-navier-stokes/
# (step 8)
#
# @author il
# -----------------------------------------------------------------------------

from mpl_toolkits.mplot3d import Axes3D # New library
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('RESULT.csv', delimiter=',')
u = data[:,2]
v = data[:,3]

u = np.reshape(u, (41,41))
v = np.reshape(v, (41,41))

x = np.linspace(0,2,len(u))
y = np.linspace(0,2,len(v))

fig = plt.figure(figsize=(11,7), dpi=100)
ax = fig.gca(projection='3d')
X,Y = np.meshgrid(x,y)
ax.plot_wireframe(X,Y,u)
wire1 = ax.plot_wireframe(X,Y,u)
wire2 = ax.plot_wireframe(X,Y,v)

plt.show()
