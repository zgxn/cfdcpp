/*!
Class that solves the 2 dimensional Burgers' equation. In 
two dimensions, Burgers' equation becomes a coupled set of 
partial differential equations (PDE):

d_t u + u d_x u + v d_y u = nu (d^2_x u + d^2_y u) -> [1]
d_t v + u d_x v + v d_y v = nu (d^2_x v + d^2_y v) -> [2]

Burgers' equation creates shocks from initially smooth data. 
In fact, the equations we are solving are the so-called 
"viscous Burgers' equations", because they contain the 
dissipative right side.

@author il
*/

#include "Burgers2D.hpp"
#include <omp.h>

/*!
Construct Burgers' equation with specified parameters

Input:
	xL : left x-coordinate
	xR : right x-coordinate
	yL : left y-coordinate
	yR : right y-coordinate
	nx : number of nodes in x-direction
	ny : number of nodes in y-direction
	nu : viscosity

Output:
	Burgers' equation
*/
Burgers2D::Burgers2D(double xL, double xR, double yL, double yR,
		int nx, int ny, double nu)
{
	fdm = FiniteDifferences();
	mnx = nx;
	mny = ny;
	mdx = (xR-xL) / (nx-1.0);
	mdy = (yR-yL) / (ny-1.0);
	mnu = nu;

	muN = new double* [mny];
	mvN = new double* [mny];

	for ( int i = 0; i < mny; i ++ ) {
		muN[i] = new double [mnx];
		mvN[i] = new double [mnx];
	}
}

/*!
Solve Burgers' equation with finite differences

Input:
	u  : [Input/Output] velocity in x-direction
	v  : [Input/Output] velocity in y-direction
	dt : time step size
*/
void Burgers2D::solve(double** u, double** v, double dt)
{

	// The boundary treatment is basically setting every cell equal to their 
	// previous value and then excluding the boundaries from the loop.
	// We can parallelize that though...
#pragma omp parallel for
	for (int i = 0; i < mny; i++) {
		for (int j = 0; j < mnx; j++) {
			muN[i][j] = u[i][j];
			mvN[i][j] = v[i][j];
		}
	}

	// Inside the domain
#pragma omp parallel for
	for (int i = 1; i < mny - 1; i++) {
		for (int j = 1; j < mnx - 1; j++) {
			muN[i][j] = update(dt, u[i][j], u[i][j], v[i][j], 
					u[i][j+1], u[i][j-1], u[i+1][j], u[i-1][j], mdx, mdy, mnu);
			mvN[i][j] = update(dt, v[i][j], u[i][j], v[i][j], 
					v[i][j+1], v[i][j-1], v[i+1][j], v[i-1][j], mdx, mdy, mnu);
		}
	}

#pragma omp parallel for
	for (int i = 1; i < mny - 1; i++) {
		for (int j = 1; j < mnx - 1; j++) {
			u[i][j] = muN[i][j];
			v[i][j] = mvN[i][j];
		}
	}
}

/*!
Returns the updated value of Burgers' equation

Calculates the value at the next time level using finite differences. 
In fact, this method uses the class FiniteDifferences.

Input:
	dt     : time step size
	q      : data at point (i,j), this can be either u or v (cf. below)
	u      : data in x-direction
	v      : data in y-direction
	qNextX : data at (i+1,j)
	qPrevX : data at (i-1,j)
	qNextY : data at (i,j+1)
	qPrevY : data at (i,j-1)
	dx     : mesh spacing in x-direction
	dy     : mesh spacing in y-direction
	nu     : viscosity

Output:
	data at (i,j) at new time level
*/
double Burgers2D::update(double dt, double q, double u, double v, 
		double qNextX, double qPrevX, double qNextY, double qPrevY, 
		double dx, double dy,	double nu)
{
	return q - dt*q*fdm.backward(q,qPrevX,dx) 
		- dt*v*fdm.backward(q,qPrevY,dy) 
		+ (nu*dt)*fdm.second(q,qNextX,qPrevX,dx)
		+ (nu*dt)*fdm.second(q,qNextX,qPrevX,dx);
}

/*!
Clear memory
*/
void Burgers2D::clearMemory()
{
	for (int i = 0; i < mny; i ++) {
		delete[] muN[i];
		delete[] mvN[i];
	}
}
