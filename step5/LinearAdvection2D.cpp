/*!
Class that solves the 2 dimensional linear advection equation
d_t u + c d_x u + c d_y u = 0

The spatial derivatives are solved via backward differencing, 
the second order derivative is solved with the Taylor 
approximation.

Boundaries are periodical.

@author il
*/

#include "LinearAdvection2D.hpp"
#include <omp.h>

#include <iostream>
/*!
Construct 2 dimensional LAE

Input:
	xL : left most coordinate x
	xR : right most coordinate x
	yL : left most coordinate y
	yR : right most coordinate y
	nx : number of nodes in x direction
	ny : number of nodes in y direction
	c  : wave speed
*/
LinearAdvection2D::LinearAdvection2D(double xL, double xR, double yL, 
double yR, int nx, int ny, double c)
{
	mnx = nx;
	mny = ny;
	mc  = c;
	mdx = (xR - xL) / nx;
	mdy = (yR - yL) / ny;
}

/*
Update the whole domain

Overwrite the input vector of data

Input:
	u  : Vector of data [INPUT/OUTPUT]
	dt : Time step size

Output:
	u  : Vector of data updated to t+dt
*/
void LinearAdvection2D::solve(double** u, double dt)
{

	// Initialise the grid
	double** uN;
	uN = new double* [mnx];
	for (int i = 0; i < mnx; i ++) {
		uN[i] = new double [mny];
	}

	// Update
#pragma omp parallel for
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {

			// Boundary conditions
			if ( i==0 and j!=0 ) {
				uN[0][j] = update(dt, u[i][j], u[mnx-1][j], u[i][j-1]);
			} else if ( j==0 and i!=0 ) {
				uN[i][0] = update(dt, u[i][j], u[i-1][j], u[i][mny-1]);
			} else if ( j==0 and i==0 ) {
				uN[0][0] = update(dt, u[i][j], u[mnx-1][j], u[i][mny-1]);
			} else {
				// Inside the domain
				uN[i][j] = update(dt, u[i][j], u[i-1][j], u[i][j-1]);
			}

		}
	}

	// Overwrite the old vector of variables
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {
			u[i][j] = uN[i][j];
		}
	}

	// Clear memory
	for (int i = 0; i < mnx; i++) {
		delete[] uN[i];
	}

}

/*!
Advance solution in time

Input:
	dt     : time step size
	u      : data at point i, j
	uNextX : data at point i+1, j
	uPrevX : data at point i-1, j
	uNextY : data at point i, j+1
	uPrevY : data at point i, j-1

Output:
	Value of data at time t+1 at point (i, j)
*/
double LinearAdvection2D::update(double dt, double u, double uPrevX, 
		double uPrevY)
{
	return u - mc*dt*backwardDifference(u, uPrevX, mdx)
		- mc*dt*backwardDifference(u, uPrevY, mdy);
}

/*!
Calculate first order differential by backward diff

Input:
	u     : data at point i
	uPrev : data at point i-1
	dL    : distance between points

Output:
	d_L u
*/
double LinearAdvection2D::backwardDifference(double u, double uPrev, double dL)
{
	return ( u - uPrev ) / dL;
}
