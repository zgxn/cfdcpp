/**
 * @name sputhelper.h
 * @brief Extensions for sput Unit Test Suite
 * @author dfa
 * @date 02.06.2015
 *
 * Everything to make the sput Unit Test Suite more convenien.
 * @see http://www.lingua-systems.com/unit-testing/
 **/

#pragma once

#include <string>
#include <iostream>
#include <sstream>

namespace dfa
{
  /**
   * @name writeTstMsg
   * @brief Return formatted Expected, Result msg
   * @details Convert input values (float, int, etc.) to a string
   *  and add the Name and maybe further informations to it. 
   *  Return whole string as char because sput likes char and hate string.
   *
   *  Example Usage:
   *  @code
   *    std::string msg = "Testname XY ";
   *    sput_fail_unless(result == 4, dfa::writeTstMsg(<EXP>, <RES>, msg));
   *  @endcode
   *
   * @param [in] expected value
   * @param [in] result value
   * @param [in] message informtaions about the test etc.
   * @return Testmessage with expected and real result values
   **/
  template<typename  E, typename R>
  static const char* writeTstMsg(E e, R r, std::string& msg)
    {
      std::ostringstream oss;
      oss << msg << " Expected: " << e << " Result: " << r;
      return strdup(oss.str().c_str());
    }

  /**
   * @name to_string
   * @brief Return inserted data as string
   * @details Stolen from stackflow
   *
   * Example Usage:
   * @code
   *   std::string msg = dfa::to_string(<INTEGER>);
   * @endcode
   *
   * @param [in] data to return as string
   * @return string
   **/
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }

} // end namespace
