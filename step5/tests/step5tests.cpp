/**
 * @file step5tests.cpp
 * @date 03.03.2016
 * @brief Tests around the CFD_CPP project step5 
 **/

#include "../LinearAdvection2D.hpp"
#include "include/sput.h"
#include "include/sputhelper.h"

#include <iostream>
#include <math.h>

const double EPSILON = 0.0000001; /*< Max. allowed diff. */

/**
 * @name test_simpleOK
 * @brief A test to test that the test is testable and is OK
 * @todo Diese Function kann und sollte geloescht werden
 **/
static void test_simpleOK()
{
  double expectedValue = 1.0;
  double realValue     = 1.0;

  std::string msg = "This test is OK";

  sput_fail_unless(
      expectedValue == realValue, 
      dfa::writeTstMsg(expectedValue, realValue, msg));
}

/**
 * @name test_simpleKO
 * @brief A test to test that the test is testable and fails
 * @todo Diese Function kann und sollte geloescht werden
 **/
static void test_simpleKO()
{
  double expectedValue = 1.00;
  double realValue     = 0.05;

  std::string msg = "This test is KO";

  sput_fail_unless(
      expectedValue == realValue,
      dfa::writeTstMsg(expectedValue, realValue, msg));
}

/**
 * @name test_backwardDifference
 * @brief Test LinearAdvection2D::backwardDifference
 *
 * Die Methode ist private und so nicht erreichbar.
 **/

static void test_backwardDifference()
{
  std::string msg = "";

  LinearAdvection2D lae ( 0.0, 0.0, 0.0, 0.0, 0, 0, 0);

  const size_t N = 3;
  double expected[N] = {-0.05, 0.0000066666666, -0.00239999999};
  double u[N] = {0.51, 0.13, 0.256};
  double uPrev[N] = {0.52, 0.12999, 0.2566};
  double dl[N] = {0.2, 1.5, 0.25};

  double result = 0.0;

  for(size_t n = 0; n < N; n++)
  {
    
    //result = lae.backwardDifference(u[n], uPrev[n], dl[n]);

    sput_fail_unless(
        fabs(expected[n] - result) < EPSILON, 
        dfa::writeTstMsg(expected[n], result, msg));
  }


}

int main(int argc, char** argv)
{
  sput_start_testing();
  sput_enter_suite("step5 Test-Suite");

  sput_run_test(test_simpleOK);
  sput_run_test(test_simpleKO);
  sput_run_test(test_backwardDifference);

  sput_finish_testing();
  
  return sput_get_return_value();
}
