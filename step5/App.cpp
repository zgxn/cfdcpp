/*!
	Application file to solve the two-dimensional linear advection
	equation. 

	@author il
 */

#include "LinearAdvection2D.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>

int main(int argc, char* argv[])
{
	int nx    = 81; // Node number in x-direction
	int ny    = 81; // Node number in y-direction
	double dx = 2.0/(nx-1); // Mesh spacing in x-direction 
	double dy = 2.0/(ny-1); // Mesh spacing in y-direction
	double c  = 1.0; // Wave speed 

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double** u0;
	u0 = new double* [nx];

	for (int i = 0; i < nx; i++) {
		u0[i] = new double [ny];
	}

	double x = 0.0;
	double y = 0.0;

	for (int i = 0; i < nx; i++) {
		for (int j = 0; j < ny; j++) {
			x = i * dx;
			y = j * dy;
			if (0.5 < y and y < 1.0 and 0.5 < x and x < 1.0) {
				u0[i][j] = 2.0;
			} else {
				u0[i][j] = 1.0;
			}
		}
	}

	// --------------------------------------------------------------------------
	// Create the object
	// --------------------------------------------------------------------------
	LinearAdvection2D lae ( 0.0, 2.0, 0.0, 2.0,  nx, ny, c );

	// --------------------------------------------------------------------------
	// Simulation parameters
	// --------------------------------------------------------------------------
	double t0    = 0.0;    // Initial time
	double dt    = 0.2 * std::min(dx,dy);
	
	double tEnd; // Time horizon
	std::cout << "Prof. Barba uses tEnd: " << dt * 100.0 << "\n";
	std::cout << "Specify time horizon: ";
	std::cin  >> tEnd;


	int counter = 0;

	// --------------------------------------------------------------------------
	// Advance in time until time horizon is reached
	// --------------------------------------------------------------------------
	while (t0 < tEnd) {

		if (t0 + dt > tEnd) {
			dt = tEnd - t0;
		}

		lae.solve(u0, dt);
		t0 = t0 + dt;

		// Every 10th step, write out that everything is OK
		if (counter % 10 == 0) {
			std::cout << counter << " Still running...\n";
		}

		counter = counter + 1;
	}

	std::cout << "Simulation finished at: " << t0 << "\n";

	// --------------------------------------------------------------------------
	// Write a CSV file with x-coordinates in one column and 
	// the corresponding data in the second column
	// --------------------------------------------------------------------------
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());


	write_output << "#x, y, u\n";

	for (int i=0; i<nx; i++) {
		for (int j=0; j<ny; j++) {
			write_output << (i*dx) << ", " << (j*dy) << ", " << u0[i][j] << "\n";
		}
	}

	write_output.close();

	return 0;
}
