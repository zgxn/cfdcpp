/*!
Class that solves the 2 dimensional linear advection equation
d_t u + c d_x u + c d_y u = 0

The spatial derivatives are solved via backward differencing, 
the second order derivative is solved with the Taylor 
approximation.

Boundaries are periodical.

@author il
*/

class LinearAdvection2D
{

public:
LinearAdvection2D(double xL, double xR, double yL, double yR, int nx, int ny, 
double c);
void solve(double** u, double dt);

private:
int mnx;
int mny;
double mc;
double mdx;
double mdy;
double backwardDifference(double u, double uPrev, double dL);
double update(double dt, double u, double uPrevX, double uPrevY);

};
