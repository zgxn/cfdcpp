#/usr/bin/python

# File to plot the results
# @author il

import numpy as np
import matplotlib.pyplot as plt
import sympy
from sympy.utilities.lambdify import lambdify

# analytical solution
x, nu, t = sympy.symbols('x nu t')
phi = sympy.exp(-(x-4*t)**2/(4*nu*(t+1))) + \
sympy.exp(-(x-4*t-2*np.pi)**2/(4*nu*(t+1)))
phiprime = phi.diff(x)

u = -2*nu*(phiprime/phi)+4
ufunc = lambdify((t, x, nu), u)

nu = 0.07
ti = 2.0*np.pi/100 * nu * 100

# Read the C++ solution
q = np.genfromtxt('RESULT.csv', delimiter=',', skip_header=1)
a = []
for x0 in q[:,0]:
	a.append(ufunc(ti, x0, nu))

plt.plot(q[:,0], q[:,1], 'o', mfc='none', mec='k', label='c++')
plt.plot(q[:,0], a, 'k-', label='analytical')
#plt.legend()
plt.xlabel('x')
plt.ylabel('u')
plt.show()
