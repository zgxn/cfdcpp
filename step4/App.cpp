/*!
	Application file to solve the diffusion equation
	d_t u + u d_x u = nu d_x^2 u

	@author il
 */

#include "BurgersEquation.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>

int main(int argc, char* argv[])
{
	int n      = 101; // Node number
	double  pi = std::atan(1.0)*4.0;
	double dx  = 2.0 * pi / (n - 1);
	double nu  = 0.07; // Diffusion parameter

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	// The initial problem to this problem is rather comlicated and contains 
	// differential terms

	double* u0  = new double[n];
	double  t   = 0.0;
	double  x   = 0;

	for (int i=0; i < n; i++) {

		x = i * dx + 0.5 * dx;

		double phi      = exp( -(pow(x-4.0*t,2) / (4.0*nu*(t+1.0))) ) + 
		exp( -(pow(x-4.0*t-2.0*pi,2) / (4.0*nu*(t+1.0))) );

		double phiprime = -(-8.0*t + 2.0*x) * 
		exp( -(pow(-4.0*t + x,2))/(4*nu*(t + 1.0)) ) / (4.0*nu*(t + 1.0)) 
		- (-8.0*t + 2.0*x - 12.5663706143592) * 
		exp( -(pow(-4.0*t + x - 6.28318530717959,2)) / (4.0*nu*(t + 1.0)) ) 
		/ (4.0 * nu*(t + 1.0));

		u0[i] = - ((2.0*nu)/phi)*phiprime + 4.0;

	}

	// --------------------------------------------------------------------------
	// Create the object
	// --------------------------------------------------------------------------
	BurgersEquation brg ( 0.0, 2.0*pi, n, nu );

	// --------------------------------------------------------------------------
	// Simulation parameters
	// --------------------------------------------------------------------------
	double tEnd; // Time horizon
	std::cout << "Specify time horizon: ";
	std::cin  >> tEnd;

	double t0    = 0.0;    // Initial time
	double dt    = dx * nu;

	int counter = 0;

	// --------------------------------------------------------------------------
	// Advance in time until time horizon is reached
	// --------------------------------------------------------------------------
	while (t0 < tEnd) {

		if (t0 + dt > tEnd) {
			dt = tEnd - t0;
		}

		brg.solve(u0, dt);
		t0 = t0 + dt;

		// Every 10th step, write out that everything is OK
		if (counter % 10 == 0) {
			std::cout << counter << " Still running...\n";
		}

		counter = counter + 1;
	}

	std::cout << "Simulation finished at: " << t0 << "\n";

	// --------------------------------------------------------------------------
	// Write a CSV file with x-coordinates in one column and 
	// the corresponding data in the second column
	// --------------------------------------------------------------------------
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());


	write_output << "#x, u\n";

	for (int i=0; i<n; i++) {
		write_output << (i*dx + 0.5*dx) << ", " << u0[i] << "\n";
	}

	write_output.close();

	return 0;
}
