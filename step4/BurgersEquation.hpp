/*!
Class that solves Burgers' Equation, written as 
d_t u + u d_x u = nu d_x^2 u

The spatial derivatives are solved via backward 
differencing, the second order derivative is 
solved with the Taylor approximation.

Boundaries are periodical.

@author il
*/
class BurgersEquation
{

public:
BurgersEquation(double xL, double xR, int n, double nu);
void solve(double* u, double dt);

private:
int mn;
double mnu;
double mdx;
double backwardDifference(double u, double uPrev);
double secondOrderDiff(double uNext, double u, double uPrev);
double update(double dt, double uNext, double u, double uPrev);


};
