/*!
Class that solves Burgers' Equation, written as 
d_t u + u d_x u = nu d_x^2 u

The spatial derivatives are solved via backward 
differencing, the second order derivative is 
solved with the Taylor approximation.

Boundaries are periodical.

@author il
*/

#include "BurgersEquation.hpp"

/*!
Construct Burgers' equation

Input:
	xL : left most coordinate
	xR : right most coordinate
	n  : number of nodes
	nu : diffusion coefficient
*/
BurgersEquation::BurgersEquation(double xL, double xR, int n, double nu)
{
	mn  = n;
	mnu = nu;
	mdx = (xR - xL) / n;
}

/*
Update the whole domain

Overwrite the input vector of data

Input:
	u  : Vector of data [INPUT/OUTPUT]
	dt : Time step size

Output:
	u  : Vector of data updated to t+dt
*/
void BurgersEquation::solve(double* u, double dt)
{

	double* uN = new double[mn];

	// Update the domain
	for (int i = 1; i < mn - 1; i ++) {
		uN[i] = update(dt, u[i+1], u[i], u[i-1]);
	}
	// Update boundaries
	uN[0] = update(dt, u[1], u[0], u[mn-1]);
	uN[mn-1] = update(dt, u[0], u[mn-1], u[mn-2]);

	// Overwrite the old vector of variables
	for (int i = 0; i < mn; i ++) {
		u[i] = uN[i];
	}

	delete[] uN;

}

/*!
Advance solution in time

Input:
	dt    : time step size
	uNext : data at point i+1
	u     : data at point i
	uPrev : data at point i-1

Output:
	Value of data at time t+1 at point i
*/
double BurgersEquation::update(double dt, double uNext, double u, double uPrev)
{
	return u - u*dt*backwardDifference(u, uPrev) + 
											mnu*dt*secondOrderDiff(uNext, u, uPrev);
}

/*!
Calculate first order differential by backward diff

Input:
	u     : data at point i
	uPrev : data at point i-1

Output:
	d_x u
*/
double BurgersEquation::backwardDifference(double u, double uPrev)
{
	return ( u - uPrev ) / mdx;
}

/*!
Calculate second differential using finite difference

Input:
	uNext : data at point i+1
	u     : data at point i
	uPrev : data at point i-1

Output:
	d_x^2 u
*/
double BurgersEquation::secondOrderDiff(double uNext, double u, double uPrev)
{
	return (uNext - 2.0*u + uPrev) / (mdx*mdx);
}
