/*!
2 dimensional Poisson's equation

d^2_x p + d^2_y p = b

Poisson's equation is Laplace's equation extended by 
a source term. There is again no time dependency and 
we will iterate to a stationary state.

@author il
*/

#ifndef __POISSO_INCLUDED__
#define __POISSO_INCLUDED__

#include "PartialDifferentialEquation.hpp"

class Poisson2D : public PartialDifferentialEquation
{

	public:
		Poisson2D(
			double xL, double xR, double yL, double yR, int nx, int ny, 
			double nmu);

	private:
		double update(
			double dt, double q, double u, double v, double s,
			double qNextX, double qPrevX, double qNextY, 
			double qPrevY, double dx,	double dy, double nu);
};

#endif
