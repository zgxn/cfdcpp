/*!
Abstract class to describe a (system of) partial differential equation(s)

Describe a (system of) partial differential equation(s) (PDE), e.g. Laplace, 
Burgers', shallow water equations. The derivatives are solved using 
finite difference approximation. 

The method update is a ``virtual'' method, because this is the method 
that actually solves the PDE by calling the finite difference methods.

@author il
*/
#include "PartialDifferentialEquation.hpp"
#include <omp.h>

/*!
Construct partial differential equation with specified parameters

Input:
	xL : left x-coordinate
	xR : right x-coordinate
	yL : left y-coordinate
	yR : right y-coordinate
	nx : number of nodes in x-direction
	ny : number of nodes in y-direction
	nu : viscosity

Output:
	Partial differential equation 
*/
PartialDifferentialEquation::PartialDifferentialEquation(double xL, 
double xR, double yL, double yR, int nx, int ny, double nu)
{
	fdm = FiniteDifferences();
	mnx = nx;
	mny = ny;
	mdx = (xR-xL) / (nx-1.0);
	mdy = (yR-yL) / (ny-1.0);
	mnu = nu;

	muN = new double* [mny];
	mvN = new double* [mny];

	for ( int i = 0; i < mny; i ++ ) {
		muN[i] = new double [mnx];
		mvN[i] = new double [mnx];
	}
}

/*!
Solve partial differential equation with finite differences

Input:
	u  : [Input/Output] velocity in x-direction
	v  : [Input/Output] velocity in y-direction
	dt : time step size
*/
void PartialDifferentialEquation::solve(double** u, double** v, 
	double** s, double dt)
{

	// The boundary treatment is basically setting every cell equal to their 
	// previous value and then excluding the boundaries from the loop.
	// We can parallelize that though...
#pragma omp parallel for
	for (int i = 0; i < mny; i++) {
		for (int j = 0; j < mnx; j++) {
			muN[i][j] = u[i][j];
			mvN[i][j] = v[i][j];
		}
	}

	// Inside the domain
#pragma omp parallel for
	for (int i = 1; i < mny - 1; i++) {
		for (int j = 1; j < mnx - 1; j++) {
			muN[i][j] = update(dt, u[i][j], u[i][j], v[i][j], s[i][j],
					u[i][j+1], u[i][j-1], u[i+1][j], u[i-1][j], mdx, mdy, mnu);
			mvN[i][j] = update(dt, v[i][j], u[i][j], v[i][j], s[i][j],
					v[i][j+1], v[i][j-1], v[i+1][j], v[i-1][j], mdx, mdy, mnu);
		}
	}

#pragma omp parallel for
	for (int i = 1; i < mny - 1; i++) {
		for (int j = 1; j < mnx - 1; j++) {
			u[i][j] = muN[i][j];
			v[i][j] = mvN[i][j];
		}
	}
}

/*!
Clear memory
*/
void PartialDifferentialEquation::clearMemory()
{
	for (int i = 0; i < mny; i ++) {
		delete[] muN[i];
		delete[] mvN[i];
	}
}
