/*!
2 dimensional Poisson's equation

See header file for more information

@author il
*/

#include "Poisson2D.hpp"

Poisson2D::Poisson2D(
	double xL, double xR, double yL, 
	double yR, int nx, int ny, double nu) 
: PartialDifferentialEquation(xL, xR, yL, yR, nx, ny, nu)
{
}

/*!
Returns the updated value of Poisson's equation

Again, this is not a hyperbolic problem, thus we cannot utilize 
the FiniteDifferences class to full extent.

Input:
	dt     : time step size
	q      : data at point (i,j), this can be either u or v (cf. below)
	u      : data in x-direction
	v      : data in y-direction
	s      : source term at (i,j)
	qNextX : data at (i+1,j)
	qPrevX : data at (i-1,j)
	qNextY : data at (i,j+1)
	qPrevY : data at (i,j-1)
	dx     : mesh spacing in x-direction
	dy     : mesh spacing in y-direction
	nu     : viscosity

Output:
	data at (i,j) at new iteration step
*/
double Poisson2D::update(
		double dt, double q, double u, double v, double s,
		double qNextX, double qPrevX, double qNextY, 
		double qPrevY, double dx,	double dy, double nu)
{
	return (dy*dy*(qNextX+qPrevX)+dx*dx*(qNextY+qPrevY)-s*dx*dx*dy*dy)
					/(2.0*(dx*dx+dy*dy));
}
