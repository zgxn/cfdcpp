/*!
Finite differences approximations for derivatives.

Backward, forward, centered differences and second order derivative. 
The forward and backward differences are first order accurate, the 
central difference is second order accurate. 

@author il
*/

#ifndef __FINDIF_INCLUDED__
#define __FINDIF_INCLUDED__

class FiniteDifferences
{
	public:
		FiniteDifferences();
		double backward(double u, double uP, double dL);
		double forward (double u, double uN, double dL);
		double centered(double uN, double uP, double dL);
		double second  (double u, double uN, double uP, double dL);
};

#endif
