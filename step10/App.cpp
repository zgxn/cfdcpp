/*!
	Starter object to solve Poisson's equations in two dimensions.

	@author il
 */

#include "Poisson2D.hpp"
#include "PartialDifferentialEquation.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>
#include <omp.h>

int main(int argc, char* argv[])
{

	// --------------------------------------------------------------------------
	// Variable declaration
	// --------------------------------------------------------------------------
	int nx = 50; // number of nodes in x-direction
	int ny = 50; // number of nodes in y-direction
	double dx = 2.0/(nx-1); // mesh spacing in x-direction
	double dy = 1.0/(ny-1); // mesh spacing in y-direction

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double** p;
	double** sour = new double* [ny]; // Source term

	p = new double* [ny];

	for (int i = 0; i < nx; i ++) {
		p[i] = new double [nx];
		sour[i] = new double [nx];
	}

	for (int i = 0; i < ny; i++) {
		for (int j = 0; j < nx; j++) {
			p[i][j] = 0.0;
			sour[i][j] = 0.0;
		}
	}

	sour[ny/4][nx/4] = 100.0;
	sour[3*ny/4][3*nx/4] = -100.0;

	// --------------------------------------------------------------------------
	// Object creation
	// --------------------------------------------------------------------------
	Poisson2D poi = Poisson2D (0.0, 2.0, 0.0, 1.0, nx, ny, 0.0);

	int t0 = 0; // Step 0
	double err = 1.0;

	double** uOld = new double* [ny]; // Store old time step here
	double** tmp  = new double* [ny]; // Store old time step here

	for (int i = 0; i < ny; i++) {
		uOld[i] = new double [nx];
		tmp[i]  = new double [nx];
	}

	// --------------------------------------------------------------------------
	// Iterate to steady state
	// --------------------------------------------------------------------------
	while (err > 1.0e-10) {

		if (t0 % 10 == 0) {
			std::cout << t0 << " | Still running... | " << err << "\n";
		}

		for (int i=0; i<ny; i++) {
			for (int j=0; j<nx; j++) {
				uOld[i][j] = p[i][j];
			}
		}
	
		err = 0.0; // we need to set `err' small, because it goes into the 
						   // max-function later
		
		poi.solve(p, tmp, sour, 0.0);

		// reapply the boundary conditions
		for (int i = 0; i < ny; i++) {
			p[i][0] = 0.0;
			p[i][nx-1] = 0.0;
		}
		for (int j = 0; j < nx; j++) {
			p[0][j] = 0.0;
			p[ny-1][j] = 0.0;
		}

		for (int i=0; i<ny; i++) {
			for (int j=0; j<nx; j++) {
				err = std::max(std::abs(uOld[i][j] - p[i][j]), err);
			}
		}

		t0 = t0 + 1;

	}

	// clean up
	poi.clearMemory();

	for (int i = 0; i < ny; i++) {
		delete[] uOld[i];
	}

	std::cout << t0 << " | Simulation finished.\n";

	// write out
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());

	for (int i=0; i<ny; i++) {
		for (int j=0; j<nx; j++) {
			write_output << (j*dx) << ", " << (i*dy) << ", " << p[i][j]
				<< "\n";
		}
	}

	write_output.close();

	return 0;
}
