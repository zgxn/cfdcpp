#!/usr/bin/python

# -----------------------------------------------------------------------------
# Plot 2D data as a 3D projection
# Implemented as seen at
# http://lorenabarba.com/blog/cfd-python-12-steps-to-navier-stokes/
# (step 5)
#
# @author il
# -----------------------------------------------------------------------------

from mpl_toolkits.mplot3d import Axes3D # New library
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt
import sys

nx = 50
ny = 50
x  = np.linspace(0.0,2.0,nx)
y  = np.linspace(0.0,2.0,ny)
dx = 2.0/(nx-1)
dy = 2.0/(ny-1)

q   = np.genfromtxt('RESULT.csv', delimiter=',')

u = np.reshape(q[:,2],(ny,nx))
u = np.flipud(u)
plt.imshow(u, interpolation='None')
plt.colorbar()

isWriteEnabled = False

if isWriteEnabled:
    for i in range(len(u[0])):
        for j in range(len(u)):
            sys.stdout.write(str(u[i,j])) 
            sys.stdout.write(' ')
            sys.stdout.write('\n')

plt.show()
