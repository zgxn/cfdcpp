/*!
Class that solves the 2 dimensional diffusion equation, 
written as:
d_t u + nu d_x^2 u + nu d_y^2 u = 0

The spatial derivatives are solved via backward differencing, 
the second order derivative is solved with the Taylor 
approximation.

Boundaries are periodical.

@author il
*/

class Diffusion2D
{

public:
Diffusion2D(double xL, double xR, double yL, double yR, 
int nx, int ny, double nu);
void solve(double** u, double dt);

private:
int mnx;
int mny;
double mdx;
double mdy;
double mnu;
double centralDifference(double uNext, double u, double uPrev, double dL);
double update(double dt, double u, double uNextX, double uNextY, double uPrevX,
	double uPrevY);

};
