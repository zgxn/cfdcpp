#!/usr/bin/python

# -----------------------------------------------------------------------------
# Plot 2D data as a 3D projection
# Implemented as seen at
# http://lorenabarba.com/blog/cfd-python-12-steps-to-navier-stokes/
# (step 5)
#
# @author il
# -----------------------------------------------------------------------------

from mpl_toolkits.mplot3d import Axes3D # New library
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt

nx = 31
ny = 31
x  = np.linspace(0.0,2.0,nx)
y  = np.linspace(0.0,2.0,ny)
dx = 2.0/(nx-1)
dy = 2.0/(ny-1)

q   = np.genfromtxt('RESULT.csv', delimiter=',', skip_header=1)

u = np.reshape(q[:,2],(ny,nx))
plt.imshow(u, interpolation='None')

fig = plt.figure(figsize=(11,7), dpi=100)
ax  = fig.gca(projection='3d')

X,Y  = np.meshgrid(x,y)
ax.plot_surface(X,Y,u[:],rstride=1,cstride=1,linewidth=0,cmap=cm.coolwarm,
antialiased=True)

ax.set_zlim(1,2.5)
plt.show()
