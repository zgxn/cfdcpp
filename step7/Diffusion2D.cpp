/*!
	Class that solves the 2 dimensional diffusion equation, 
	written as:
	d_t u + nu d_x^2 u + nu d_y^2 u = 0

	The spatial derivatives are solved via backward differencing, 
	the second order derivative is solved with the Taylor 
	approximation.

	Boundaries are periodical.

	@author il
 */

#include "Diffusion2D.hpp"
#include <omp.h>

/*!
	Construct 2 dimensional non linear advection equation

Input:
xL : left most coordinate x
xR : right most coordinate x
yL : left most coordinate y
yR : right most coordinate y
nx : number of nodes in x direction
ny : number of nodes in y direction
nu : diffusion coefficient
 */
Diffusion2D::Diffusion2D(double xL, double xR, double yL, 
		double yR, int nx, int ny, double nu)
{
	mnx = nx;
	mny = ny;
	mdx = (xR - xL) / nx;
	mdy = (yR - yL) / ny;
	mnu = nu;
}

/*
	 Update the whole domain

	 Overwrite the input vector of data

Input:
u  : Vector of data in x-direction [INPUT/OUTPUT]
v  : Vector of data in y-direction [INPUT/OUTPUT]
dt : Time step size

Output:
u  : Vector of data in x-direction updated to t+dt
v  : Vector of data in y-direction updated to t+dt
 */
void Diffusion2D::solve(double** u, double dt)
{

	// Initialise the grid
	double** uN;
	uN = new double* [mnx];
	for (int i = 0; i < mnx; i ++) {
		uN[i] = new double [mny];
	}

	// This is to set the boundaries
#pragma omp parallel for
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {
			uN[i][j] = 1.0;
		}
	}

	// Update
#pragma omp parallel for
	for (int i = 1; i < mnx-1; i ++) {
		for (int j = 1; j < mny-1; j ++) {
			uN[i][j] = update(dt, u[i][j], u[i+1][j], u[i][j+1], u[i-1][j], 
					u[i][j-1]);
		}
	}

	// Overwrite the old vector of variables
	for (int i = 0; i < mnx; i ++) {
		for (int j = 0; j < mny; j ++) {
			u[i][j] = uN[i][j];
		}
	}

	// Clear memory
	for (int i = 0; i < mnx; i++) {
		delete[] uN[i];
	}

}

/*!
	Advance solution in time

Input:
dt     : time step size
u      : data at point i, j (in x-direction)
uNextX : data at point i+1, j
uNextY : data at point i, j+1
uPrevX : data at point i-1, j
uPrevY : data at point i, j-1

Output:
Value of data at time t+1 at point (i, j)
 */
double Diffusion2D::update(double dt, double u, double uNextX, 
		double uNextY, double uPrevX, double uPrevY)
{
	return u + mnu*dt*centralDifference(uNextX, u, uPrevX, mdx)
		+ mnu*dt*centralDifference(uNextY, u, uPrevY, mdy);
}

/*!
	Calculate first order differential by backward diff

Input:
u     : data at point i
uPrev : data at point i-1
dL    : distance between points

Output:
d_L u
 */
double Diffusion2D::centralDifference(double uNext, double u, double uPrev, 
		double dL)
{
	return ( uNext - 2.0*u + uPrev ) / ( dL * dL );
}
