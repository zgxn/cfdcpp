/*!
Class that solves the 2 dimensional Laplace's equation. 

d^2_x p + d^2_y p = 0

Note that there is no time dependency. The simulation 
consists of iterating to a steady state.

@author il
*/

#ifndef __LAPLAC_INCLUDED__
#define __LAPLAC_INCLUDED__

#include "PartialDifferentialEquation.hpp"

class Laplace2D : public PartialDifferentialEquation
{

	public:
		Laplace2D(double xL, double xR, double yL, double yR,
				int nx, int ny, double nmu);

	private:
		double update(double dt, double q, double u, double v, 
				double qNextX, double qPrevX, double qNextY, double qPrevY,
				double dx, double dy, double nu);

};

#endif
