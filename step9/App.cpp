/*!
	Starter object to solve the viscous Laplace equations in 
	two dimensions.

	@author il
 */

#include "Laplace2D.hpp"
#include "PartialDifferentialEquation.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>
#include <omp.h>

int main(int argc, char* argv[])
{

	// --------------------------------------------------------------------------
	// Variable declaration
	// --------------------------------------------------------------------------
	int nx = 31; // number of nodes in x-direction
	int ny = 31; // number of nodes in y-direction
	double dx = 2.0/(nx-1); // mesh spacing in x-direction
	double dy = 1.0/(ny-1); // mesh spacing in y-direction

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double** p;

	p = new double* [ny];

	for (int i = 0; i < nx; i ++) {
		p[i] = new double [nx];
	}

	double y = 0.0;

	for (int i = 0; i < ny; i++) {
		for (int j = 0; j < nx; j++) {
			p[i][j] = 0.0;
		}
	}

	// --------------------------------------------------------------------------
	// Boundary conditions
	// --------------------------------------------------------------------------
	for (int i = 0; i < ny; i++) {
		y = i * dy;
		for (int j = 0; j < nx; j++) {
			if (j == nx-1) { // x=2, then p=y
				p[i][j] = y;
			}
			if (i == 0) {
				p[i][j] = p[i+1][j];
			} else if (i == nx-1) {
				p[i][j] = p[i-1][j];
			}
		}
	}

	// --------------------------------------------------------------------------
	// Object creation
	// --------------------------------------------------------------------------
	Laplace2D lap = Laplace2D (0.0, 2.0, 0.0, 1.0, nx, ny, 0.0);

	int t0 = 0; // Step 0
	double err = 1.0;

	double** uOld = new double* [ny]; // Store old time step here
	double** tmp  = new double* [ny]; // Store old time step here

	for (int i = 0; i < ny; i++) {
		uOld[i] = new double [nx];
		tmp[i]  = new double [nx];
	}

	// --------------------------------------------------------------------------
	// Iterate to steady state
	// --------------------------------------------------------------------------
	while (err > 1.0e-10) {

		if (t0 % 10 == 0) {
			std::cout << t0 << " | Still running... | " << err << "\n";
		}

		for (int i=0; i<ny; i++) {
			for (int j=0; j<nx; j++) {
				uOld[i][j] = p[i][j];
			}
		}
	
		err = 0.0; // we need to set `err' small, because it goes into the 
						   // max-function later
		
		lap.solve(p, tmp, 0.0);

		// reapply the boundary conditions
		for (int i = 0; i < ny; i++) {
			y = i * dy;
			for (int j = 0; j < nx; j++) {
				if (j == nx-1) { // x=2, then p=y
					p[i][j] = y;
				}
				if (i == 0) {
					p[i][j] = p[i+1][j];
				} else if (i == nx-1) {
					p[i][j] = p[i-1][j];
				}
			}
		}

		for (int i=0; i<ny; i++) {
			for (int j=0; j<nx; j++) {
				err = std::max(std::abs(uOld[i][j] - p[i][j]), err);
			}
		}

		// err = 0.0;

		t0 = t0 + 1;

	}

	// clean up
	lap.clearMemory();

	for (int i = 0; i < ny; i++) {
		delete[] uOld[i];
	}

	std::cout << t0 << " | Simulation finished.\n";

	// write out
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());

	for (int i=0; i<ny; i++) {
		for (int j=0; j<nx; j++) {
			write_output << (j*dx) << ", " << (i*dy) << ", " << p[i][j]
				<< "\n";
		}
	}

	write_output.close();

	return 0;
}
