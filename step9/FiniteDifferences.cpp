/*!
Finite differences approximations for derivatives.

Backward, forward, centered differences and second order derivative. 
The forward and backward differences are first order accurate, the 
central difference is second order accurate. 

@author il
*/

#include "FiniteDifferences.hpp"

FiniteDifferences::FiniteDifferences()
{
}

/*!
Calculate backward difference

(u - uP) / dL

Input:
	u  : data at point i
	uP : data at point i-1
	dL : distance between points

Output:
	Backward difference
*/
double FiniteDifferences::backward(double u, double uP, double dL)
{
	return (u - uP) / dL;
}

/*!
Calculate forward difference

(uN - u) / dL

Input:
	u  : data at point i
	uN : data at point i+1
	dL : distance between points

Output:
	Forward difference
*/
double FiniteDifferences::forward(double u, double uN, double dL)
{
	return (uN - u) / dL;
}

/*!
Calculate central difference

(uN - uP) / 2*dL

Input:
	uN : data at point i+1
	uP : data at point i-1
	dL : distance between points

Output:
	Central difference
*/
double FiniteDifferences::centered(double uN, double uP, double dL)
{
	return (uN - uP) / (2.0*dL);
}

/*!
Calculate central difference

(uN - 2.0*u + uP) / dL*dL

Input:
	uN : data at point i+1
	u  : data at point i
	uP : data at point i-1
	dL : distance between points

Output:
	Central difference
*/
double FiniteDifferences::second(double u, double uN, double uP, double dL)
{
	return (uN - 2.0*u + uP) / (dL*dL);
}
