/*!
Class that solves the 2 dimensional Laplace equation. 

d^2_x p + d^2_y p = 0

Note that there is no time dependency. The simulation 
consists of iterating to a steady state.

@author il
*/

#include "Laplace2D.hpp"

Laplace2D::Laplace2D(double xL, double xR, double yL, double yR,
				int nx, int ny, double nu)
: PartialDifferentialEquation(xL, xR, yL, yR, nx, ny, nu)
{
}

/*!
Returns the updated value of Laplace's equation

Calculates the value at the next time level using finite differences. 
This method differs from other implementations, as it does not use 
finite differences per se. Instead, the two second order derivatives 
are approximated by FDM but the equation is reformulated to obtain 
the updated value of q. 

Thus, we cannot use the FiniteDifferences class.

Input:
	dt     : time step size
	q      : data at point (i,j), this can be either u or v (cf. below)
	u      : data in x-direction
	v      : data in y-direction
	qNextX : data at (i+1,j)
	qPrevX : data at (i-1,j)
	qNextY : data at (i,j+1)
	qPrevY : data at (i,j-1)
	dx     : mesh spacing in x-direction
	dy     : mesh spacing in y-direction
	nu     : viscosity

Output:
	data at (i,j) at new iteration step
*/
double Laplace2D::update(double dt, double q, double u, double v, 
		double qNextX, double qPrevX, double qNextY, double qPrevY, 
		double dx, double dy,	double nu)
{
	return (dy*dy*(qNextX+qPrevX)+dx*dx*(qNextY+qPrevY))/(2.0*(dx*dx+dy*dy));
}
