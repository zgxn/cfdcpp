/*!
	Solves the linear advection equation (LAE) using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + c d_x u = 0
	and represents the propagation of a wave with speed c.
	With the initial condition u(x,0) = u0(x), the exact solution is u0(x-ct).

	@author il
*/

// LinearAdvection.h
#ifndef __LINADV_INCLUDED__ // if LinearAdvection.h hasn't been included
#define __LINADV_INCLUDED__ // define it, so the compiler knows it has been 
														// included

#include <cmath>

class LinearAdvection
{

public:
	LinearAdvection(double cfl, double xL, double xR, int n, double c, 
	double* u0);
	double* solve();
	double getTimestep();

private:
	double* u;
	double* uN;
	int mn;
	double mc, mdx, mdt;
	double backwardDifference(double dx, double u, double uPrev);
	double update(double u, double uPrev);

};

#endif
