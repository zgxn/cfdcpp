import numpy as np

def f(t):
	dx = 2.0 / 1000
	x = np.linspace(0.0+0.5*dx, 2.0+0.5*dx, 1001)
	u = np.ones(1001)
	u[.5/dx : 1/dx+1] = 2
	c = 1.0
	r = u.copy()

	for i in range(1000):
		x0 = x[i] - c*t
		if x0 < 1.0 and 0.5 < x0:
			r[i] = 2.0
		else:
			r[i] = 1.0

	return x, r
