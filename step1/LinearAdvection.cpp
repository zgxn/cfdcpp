/*!
	Solves the linear advection equation (LAE) using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + c d_x u = 0
	and represents the propagation of a wave with speed c.
	With the initial condition u(x,0) = u0(x), the exact solution is u0(x-ct).

	@author il
*/

#include <cmath>
#include "LinearAdvection.hpp"

/*!
	Construct a LinearAdvection problem by specifying its parameters.

	Input:
		xL : Left boundary coordinate
		xR : Right boundary coordinate
		n  : Number of nodes
		c  : Wave speed
		u0 : Initial condition
*/
LinearAdvection::LinearAdvection(double cfl, double xL, double xR, 
int n, double c, double* u0)
{
	mc  = c;
	mn  = n;
	mdx = (xR - xL) / (mn - 1);
	u   = new double[mn];
	uN  = new double[mn];

	// Initial conditions
	for (int i=0; i<n; i++) {
		u[i] = u0[i];
	}

	// Calculate time step using the CFL number
	mdt = cfl * mdx / std::abs(c);
}

/*!
	Gives the time step size

	Time step is calculated by the CFL stability criteria
*/
double LinearAdvection::getTimestep() {
	return mdt;
}

/*!
	Solve the linear advection equation using backward difference in space
	and forward difference in time.

	Input:
		No input required. The vectors are stored in the object.
	Output:
		Solution vector
*/
double* LinearAdvection::solve()
{
	// Periodic boundary conditions
	uN[0] = update(u[0], u[mn-1]);

	// Update all variables inside the domain
	for (int i=1; i < mn; i++) {
		uN[i] = update(u[i], u[i-1]);
	}

	// Replace the old vector of variables with 
	// the new one.
	for (int i=0; i < mn; i++)   {
		u[i] = uN[i];
	}

	return u;
}


/*!
	Calculate backward difference as:
	d_x u = ( u(x+dx) - u(x) ) / dx

	Input:
		dx    : Distance between points
		u     : Value at point i
		uPrev : Value at point i-1
	Output:
		Value of the backward difference
*/
double LinearAdvection::backwardDifference(double dx, double u, double uPrev)
{
	return ( u - uPrev ) / dx;
}

/*!
	Advance in time using a forward difference as:
	d_t u = ( u^n+1 - u^n ) / dt

	Input:
		dx    : Distance between points
		dt    : Time step size
		u     : Value at point i
		uPrev : Value at point i-1
	Output:
		Value at the new time level
*/
double LinearAdvection::update(double u, double uPrev)
{
	return u - mc * mdt * backwardDifference(mdx, u, uPrev);
}
