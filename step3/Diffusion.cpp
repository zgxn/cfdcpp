/*!
	Solves the diffusion equation using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + nu d_x^2 u = 0

	@author il
*/

#include <cmath>
#include "Diffusion.hpp"

/*!
	Construct a Diffusion problem by specifying its parameters.

	Input:
		xL : Left boundary coordinate
		xR : Right boundary coordinate
		n  : Number of nodes
		c  : Wave speed
		u0 : Initial condition
*/
Diffusion::Diffusion(double xL, double xR, 
int n, double* u0, double nu)
{
	mnu = nu; // Diffusion coefficient
	mn  = n;
	mdx = (xR - xL) / (mn - 1);
	u   = new double[mn];
	uN  = new double[mn];

	// Initial conditions
	for (int i=0; i<n; i++) {
		u[i] = u0[i];
	}

}

/*!
	Solve the diffusion equation using centred difference in space
	and forward difference in time. Uses a constant time step to update the 
	solution.

	Input:
		dt : Time step size
	Output:
		Solution vector
*/
double* Diffusion::solve(double dt)
{
	// Periodic boundary conditions
	uN[0] = update(u[0], u[mn-1], u[1], dt);
	uN[mn-1] = update(u[mn-1], u[mn-2], u[0], dt);

	// Update all variables inside the domain
	for (int i=1; i < mn-1; i++) {
		uN[i] = update(u[i], u[i-1], u[i+1], dt);
	}

	// Replace the old vector of variables with 
	// the new one.
	for (int i=0; i < mn; i++)   {
		u[i] = uN[i];
	}

	return u;
}


/*!
	Calculate the second order derivative using centred difference as:
	d_x^2 u = ( u(x+dx) - 2 u(x) + u(x-dx) ) / dx^2

	Input:
		dx    : Distance between points
		u     : Value at point i
		uPrev : Value at point i-1
		uNext : Value at point i+1
	Output:
		Value of the backward difference
*/
double Diffusion::secondDerivativeDifference(double dx, double u, 
double uPrev, double uNext)
{
	return ( uNext - 2*u + uPrev ) / (dx*dx);
}

/*!
	Advance in time using a forward difference as:
	d_t u = ( u^n+1 - u^n ) / dt

	Input:
		u     : Value at point i
		uPrev : Value at point i-1
		dt    : Time step size
	Output:
		Value at the new time level
*/
double Diffusion::update(double u, double uPrev, double uNext, double dt)
{
	return u + mnu * dt * secondDerivativeDifference(mdx, u, uPrev, uNext);
}
