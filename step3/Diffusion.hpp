/*!
	Solves the diffusion equation using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + nu d_x^2 u = 0
	and represents the diffusion of the quantity u, governed 
	by the diffusion coefficient nu.

	@author il
*/

#ifndef __DIFFUSION_INCLUDED__ // if Diffusion.h hasn't been included
#define __DIFFUSION_INCLUDED__ // define it, so the compiler knows it has been 
						  								 // included

#include <cmath>

class Diffusion
{

public:
	Diffusion(double xL, double xR, int n, double* u0, double nu);
	double* solve(double dt);

private:
	double* u;
	double* uN;
	int mn;
	double mnu;
	double mdx;
	double secondDerivativeDifference(double dx, double u, double uPrev, 
	double uNext);
	double update(double u, double uPrev, double uNext, double dt);

};

#endif
