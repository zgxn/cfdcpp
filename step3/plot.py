#/usr/bin/python

# File to plot the results
# @author il

import numpy as np
import matplotlib.pyplot as plt

# -----------------------------------------------------------------------------
# Calculate the reference solution from L. Barba
# -----------------------------------------------------------------------------
nx = 100
dx = 2.0/(nx-1)
nt = 20    #the number of timesteps we want to calculate
nu = 0.3   #the value of viscosity
sigma = .2 #sigma is a parameter, we'll learn more about it later
dt = sigma*dx**2/nu #dt is defined using sigma ... more later!

print 'time horizon: ', nt*dt

u = np.ones(nx)      #a numpy array with nx elements all equal to 1.

for i in range(nx):
	if ((i*dx + 0.5*dx) > 0.5) and ((i*dx + 0.5*dx) < 1.0):
		u[i] = 2.0

for n in range(nt):  #iterate through time
	un = u.copy() ##copy the existing values of u into un
	for i in range(1,nx-1):
		u[i] = un[i] + nu*dt/dx**2*(un[i+1]-2*un[i]+un[i-1])

# Done.
# Read the C++ solution
q = np.genfromtxt('RESULT.csv', delimiter=',', skip_header=1)
plt.plot(q[:,0], q[:,1], 'o', mfc='none', mec='k', label='c++')
plt.plot(q[:,0], u, 'k-', label='L. Barba')
plt.legend()
plt.xlabel('x')
plt.ylabel('u')
plt.show()
