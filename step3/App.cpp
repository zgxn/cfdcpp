/*!
	Application file to solve the diffusion equation
	d_t u + nu d_x^2 u = 0

	using the implemented Diffusion class.#

	@author il
 */

#include "Diffusion.hpp"
#include <iostream>
#include <fstream>
#include <cassert>

int main(int argc, char* argv[])
{
	int n = 100;
	double dx = 2.0 / (n - 1);
	double nu = 0.3; // Diffusion parameter

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double* u0 = new double[n];
	for (int i=0; i < n; i++) {
		if (i*dx+0.5*dx > 0.5 and i*dx+0.5*dx < 1.0) {
			u0[i] = 2.0;
		} else {
			u0[i] = 1.0;
		}
	}

	// --------------------------------------------------------------------------
	// Create the object
	// --------------------------------------------------------------------------
	Diffusion dif ( 0.0, 2.0, n, u0, nu );

	// --------------------------------------------------------------------------
	// Simulation parameters
	// --------------------------------------------------------------------------
	double tEnd; // Time horizon
	std::cout << "Specify time horizon: ";
	std::cin  >> tEnd;

	double t0    = 0.0;    // Initial time
	double* r = new double[n]; // Store the result here in this vector
	double sigma = 0.2;    // A stability parameter
	double dt    = sigma * dx * dx / nu;

	int counter = 0;

	// --------------------------------------------------------------------------
	// Advance in time until time horizon is reached
	// --------------------------------------------------------------------------
	while (t0 < tEnd) {

		if (t0 + dt > tEnd) {
			dt = tEnd - t0;
		}

		r = dif.solve(dt);
		t0 = t0 + dt;

		// Every 10th step, write out that everything is OK
		if (counter % 10 == 0) {
			std::cout << counter << " Still running...\n";
		}

		counter = counter + 1;
	}

	std::cout << "Simulation finished at: " << t0 << "\n";

	// --------------------------------------------------------------------------
	// Write a CSV file with x-coordinates in one column and 
	// the corresponding data in the second column
	// --------------------------------------------------------------------------
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());


	write_output << "#x, u\n";

	for (int i=0; i<n; i++) {
		write_output << (i*dx + 0.5*dx) << ", " << r[i] << "\n";
	}

	write_output.close();

	delete [] r;

	return 0;
}
