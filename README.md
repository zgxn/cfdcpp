Brief
-----
This is a C++ implementation of the course 'CFD Python: 12 steps to Navier-
Stokes' by Prof. Lorena A. Barba at
http://lorenabarba.com/blog/cfd-python-12-steps-to-navier-stokes/

It is basically a collection of finite difference based solutions to 
several types of partial differential equations.

Requirements
------------
* I tested and compiled the C++ files with the GNU compiler 
version 4.7.2 for Debian 8
* OpenMP parallelisation uses libgomp1 version 4.4.1-1 on Debian 8
* Python files are used for visualization and written in Python 2.7
* Python files use packages NumPy, PyPlot and matplotlib
* For Sput Unit Testing Framework no external dependencies have to be resolved

Usage
-----
If you would like to run the examples, compile it using the Makefile, i.e.
'make' should compile all necessary files inside a folder. Older steps 
require 'make all', confer to the corresponding Makefile.
Unit testing is not yet implemented.
