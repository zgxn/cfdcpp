/*!
Abstract class to describe a (system of) partial differential equation(s)

Describe a (system of) partial differential equation(s) (PDE), e.g. Laplace, 
Burgers', shallow water equations. The derivatives are solved using 
finite difference approximation. 

The method update is a ``virtual'' method, because this is the method 
that actually solves the PDE by calling the finite difference methods.

@author il
*/

#ifndef __PARDIF_INCLUDED__
#define __PARDIF_INCLUDED__

#include "FiniteDifferences.hpp"

class PartialDifferentialEquation
{

	public:
		PartialDifferentialEquation(
			double xL, double xR, double yL, double yR,
			int nx, int ny, double nu);
		void solve(double** u, double** v, double** s, double dt);
		void clearMemory();

	protected:
		FiniteDifferences fdm;
		int mnx;
		int mny;
		double mdx;
		double mdy;
		double mnu;
		double** muN;
		double** mvN;
		double** msN;

	private:
		virtual double update(
			double dt, double q, double u, double v, double s, 
			double qNextX, double qPrevX, double qNextY, double qPrevY,
			double dx, double dy, double nu) 
				= 0;

};

#endif
