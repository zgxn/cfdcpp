/*
Two-dimensional Navier-Stokes equations

Describe the two-dimensional Navier-Stokes equations for 
cavity flow. The derivatives are solved using finite difference 
approximation.

@author il
*/

#ifndef __NAVSTO_INCLUDED__
#define __NAVSTO_INCLUDED__

#include "FiniteDifferences.hpp"

class NavierStokes
{

	public:

		NavierStokes(double xL, double xR, double yL,
			double yR, int nx, int ny, double nu, double rh);

	private:

		FiniteDifferences fdm;

		// grid
		int mnx;											// number of nodes in x-direction
		int mny;											// number of nodes in y-direction
		double mdx;										// size in x-direction
		double mdy;										// size in y-direction

		double mnu;										// fluid viscosity
		double mrh;                   // fluid density

		double** muN;									// velocity in x-direction
		double** mvN;									// velocity in y-direction
		double** mpN;									// pressure

		double getNewVelocity(double dt, double dx, double dy, double v0, 
		double vP, double vN, double pP, double pN);
		double getNewPressure(double dt);

};

#endif
