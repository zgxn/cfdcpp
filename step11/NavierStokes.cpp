/*
Two-dimensional Navier-Stokes equations

Describe the two-dimensional Navier-Stokes equations for 
cavity flow. The derivatives are solved using finite difference 
approximation.

@author il
*/

#include "NavierStokes.hpp"
#include "PartialDifferentialEquation.hpp"

/*
 * Construct a 2D Navier-Stokes problem
 *
 * In:
 *	xL : left x-coordinate
 *	xR : right x-coordinate
 *	yL : left y-coordinate
 *	yR : right y-coordinate
 *	nx : number of nodes in x-direction
 *	ny : number of nodes in y-direction
 *	nu : turbulent viscosity
 *	rh : fluid density
 *
 * Out:
 *	Navier-Stokes problem
 */
NavierStokes::NavierStokes(
	double xL, double xR, double yL, 
	double yR, int nx, int ny, double nu, double rh)
{

	fdm = FiniteDifferences();

	mnx = nx;
	mny = ny;
	mdx = (xR-xL) / (nx-1.0);
	mdy = (yR-yL) / (ny-1.0);

	mrh = rh;
	mnu = nu;

	muN = new double* [mny];
	mvN = new double* [mny];
	mpN = new double* [mny];

	for ( int i = 0; i < mny; i ++ ) {
		muN[i] = new double [mnx];
		mvN[i] = new double [mnx];
		mpN[i] = new double [mnx];
	}
}

double NavierStokes::getNewVelocity(double dt, double dx, double dy, 
		double v0, double vP, double vN, double pP, double pN)
{
}

