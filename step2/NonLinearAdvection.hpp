/*!
	Solves the non linear advection equation (LAE) using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + u d_x u = 0
	and represents the propagation of a wave with speed u, which is the 
	transported quantity itself.

	@author il
*/

// LinearAdvection.h
#ifndef __NONLINADV_INCLUDED__ // if NonLinearAdvection.h hasn't been included
#define __NONLINADV_INCLUDED__ // define it, so the compiler knows it has been 
						  								 // included

#include <cmath>

class NonLinearAdvection
{

public:
	NonLinearAdvection(double xL, double xR, int n, double* u0);
	double* solve(double dt);

private:
	double* u;
	double* uN;
	int mn;
	double mdx;
	double backwardDifference(double dx, double u, double uPrev);
	double update(double u, double uPrev, double dt);

};

#endif
