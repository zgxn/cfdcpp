/*!
	Application file to solve the linear advection equation
	d_t u + 0.5 d_x u = 0

	using the implemented LinearAdvection class.#

	@author il
 */

#include "NonLinearAdvection.hpp"
#include <iostream>
#include <fstream>
#include <cassert>

int main(int argc, char* argv[])
{
	int n = 100;
	double dx = 2.0 / (n - 1);

	// --------------------------------------------------------------------------
	// Initial condition
	// --------------------------------------------------------------------------
	double* u0 = new double[n];
	for (int i=0; i < n; i++) {
		if (i*dx+0.5*dx > 0.5 and i*dx+0.5*dx < 1.0) {
			u0[i] = 2.0;
		} else {
			u0[i] = 1.0;
		}
	}

	// --------------------------------------------------------------------------
	// Create the object
	// --------------------------------------------------------------------------
	NonLinearAdvection nlae ( 0.0, 2.0, n, u0 );

	// --------------------------------------------------------------------------
	// Simulation parameters
	// --------------------------------------------------------------------------
	const double tEnd = 0.5; // Time horizon

	double t0   = 0.0; // Initial time
	double* r;         // Store the result here in this vector
	double dt = 0.0025; // Set constant time step

	int counter = 0;

	// --------------------------------------------------------------------------
	// Advance in time until time horizon is reached
	// --------------------------------------------------------------------------
	while (t0 < tEnd) {

		if (t0 + dt > tEnd) {
			dt = tEnd - t0;
		}

		r = nlae.solve(dt);
		t0 = t0 + dt;

		// Every 10th step, write out that everything is OK
		if (counter % 10 == 0) {
			std::cout << counter << " Still running...\n";
		}

		counter = counter + 1;
	}

	std::cout << "Simulation finished at: " << t0 << "\n";

	// --------------------------------------------------------------------------
	// Write a CSV file with x-coordinates in one column and 
	// the corresponding data in the second column
	// --------------------------------------------------------------------------
	std::ofstream write_output("RESULT.csv");
	assert(write_output.is_open());


	write_output << "#x, u\n";

	for (int i=0; i<n; i++) {
		write_output << (i*dx + 0.5*dx) << ", " << r[i] << "\n";
	}

	write_output.close();

	return 0;
}
