/*!
	Solves the linear advection equation (LAE) using finite differencing.

	The partial differential equation (PDE) has the form
	d_t u + c d_x u = 0
	and represents the propagation of a wave with speed c.
	With the initial condition u(x,0) = u0(x), the exact solution is u0(x-ct).

	@author il
*/

#include <cmath>
#include "NonLinearAdvection.hpp"

/*!
	Construct a NonLinearAdvection problem by specifying its parameters.

	Input:
		xL : Left boundary coordinate
		xR : Right boundary coordinate
		n  : Number of nodes
		c  : Wave speed
		u0 : Initial condition
*/
NonLinearAdvection::NonLinearAdvection(double xL, double xR, 
int n, double* u0)
{
	mn  = n;
	mdx = (xR - xL) / (mn - 1);
	u   = new double[mn];
	uN  = new double[mn];

	// Initial conditions
	for (int i=0; i<n; i++) {
		u[i] = u0[i];
	}

}

/*!
	Solve the non linear advection equation using backward difference in space
	and forward difference in time. Uses a constant time step to update the 
	solution.

	Input:
		dt : Time step size
	Output:
		Solution vector
*/
double* NonLinearAdvection::solve(double dt)
{
	// Periodic boundary conditions
	uN[0] = update(u[0], u[mn-1], dt);

	// Update all variables inside the domain
	for (int i=1; i < mn; i++) {
		uN[i] = update(u[i], u[i-1], dt);
	}

	// Replace the old vector of variables with 
	// the new one.
	for (int i=0; i < mn; i++)   {
		u[i] = uN[i];
	}

	return u;
}


/*!
	Calculate backward difference as:
	d_x u = ( u(x+dx) - u(x) ) / dx

	Input:
		dx    : Distance between points
		u     : Value at point i
		uPrev : Value at point i-1
	Output:
		Value of the backward difference
*/
double NonLinearAdvection::backwardDifference(double dx, double u, 
double uPrev)
{
	return ( u - uPrev ) / dx;
}

/*!
	Advance in time using a forward difference as:
	d_t u = ( u^n+1 - u^n ) / dt

	Input:
		u     : Value at point i
		uPrev : Value at point i-1
		dt    : Time step size
	Output:
		Value at the new time level
*/
double NonLinearAdvection::update(double u, double uPrev, double dt)
{
	return u - u * dt * backwardDifference(mdx, u, uPrev);
}
